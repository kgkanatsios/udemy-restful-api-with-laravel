@component('mail::message')
# Hello {{$user->name}}

Your email address has changed. Please verify your new email address using this button:

@component('mail::button', ['url' => route('verify', ['token'=>$user->verification_token])])
Verify Account
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
